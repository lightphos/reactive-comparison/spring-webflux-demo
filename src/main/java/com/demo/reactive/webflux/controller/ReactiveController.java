package com.demo.reactive.webflux.controller;

import com.demo.reactive.webflux.domain.Service;
import org.reactivestreams.Publisher;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.BodyInserters.fromObject;

@RestController
public class ReactiveController {

    @GetMapping("/services")
    public ResponseEntity serverResponseMono() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResponseEntity
                .ok()
                .header("Connection", "close")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Flux.just("test"))
                ;
    }


    @GetMapping("/list")
    public Flux<Service> list() throws Exception {
        //Thread.sleep(1000); // introduce a delay to simulate a service request response latency
        Flux<Service> f = Flux.just(
                new Service("sender"),
                new Service("receiver")
        );

        return f;
    }


}

